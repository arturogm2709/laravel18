<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            //'age' => 25,
            'name' => 'Arturo',
            'email' => 'arturo@gmail.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);
        DB::table('users')->insert([
            //'age' => rand(14,70),
            'name' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10),
        ]);


        factory(App\User::class, 50)->create(); //añadir 50 entradas en la base de datos
    }
}
