@extends('layouts.app')

@section('title', 'Lista de Usuarios')

@section('content')
<h1>Alta de usuarios</h1>
<form method="post" action="/users">

{{--     @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif --}}
    {{ @csrf_field() }}
    <label>Nombre</label>
    <input type="text" name="name" value="{{ old('name') }}">
    <div>
        {{ $errors->first('name') }}
    </div>
    <br>

    <?php
        $example=['rojo','azul','verde']
    ?>

    <select name="color">
        @foreach ($example as $item)
            <option value="{{ $item }}"
            {{ old('color') == $item ? 'selected="selected"' : '' }}>
                {{ $item }}
            </option>
        @endforeach

    </select>
    {{ old('color') }}

    <label>Email</label>
    <input type="text" name="email" value="{{ old('email') }}">
    <div>
        {{ $errors->first('email') }}
    </div>

    <br>
    <label>Contraseña</label>
    <input type="password" name="password">
    <div>
        {{ $errors->first('password') }}
    </div>
    <br>

    <input type="submit" value="Nuevo">
</form>
@endsection
