@extends('layouts.app')

@section('title', 'Lista de Usuarios')

@section('content')
<h1>Editar usuario</h1>
    <form method="post" action="/users/{{ $user->id }}">
        {{ @csrf_field() }}

        <input type="hidden" name="_method" value="PUT">
        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') ? old('name') : $user->name }}">

                <div>
            {{ $errors->first('nombre') }}
        </div>
        <br>
        <label>Email</label>
        <input type="text" name="email" value="{{ old('email') ? old('email') : $user->email }}">
        <div>
            {{ $errors->first('email') }}
        </div>
        <br>


        <input type="submit" value="Nuevo">
    </form>
@endsection
