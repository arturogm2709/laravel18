@extends('layouts.app')

@section('title', 'Lista de Usuarios')

@section('content')
<h1>Lista de usuarios</h1>

<a href="/users/create">Nuevo usuario</a>
<ul>
    @forelse($users as $user)
    <li>{{ $user->name }}: {{ $user->email }} <a href="/users/{{ $user->id }}/edit">Editar</a>
    <form method="post" action="/users/{{ $user->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" value="Borrar">

    </form>
    </li>
    @empty
    <li>No hay usuarios</li>
    @endforelse

</ul>

{{ $users->render() }}
@endsection
